package com.williamdubail.bankingtest.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ResourceResponse(

    @SerializedName("resources")
    var resourceList: List<Resource>

)