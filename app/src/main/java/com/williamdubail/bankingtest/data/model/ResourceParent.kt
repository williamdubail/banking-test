package com.williamdubail.bankingtest.data.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**

{
"id": 159,
"resource_uri": "/v2/categories/159",
"resource_type": "category"
}
 */

data class ResourceParent(

    @Expose
    var id: Long,

    @SerializedName("resource_uri")
    var uri: String,

    @SerializedName("resource_type")
    var type: String,

) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ResourceParent) return false

        if (id != other.id) return false
        if (uri != other.uri) return false
        if (type != other.type) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + uri.hashCode()
        result = 31 * result + type.hashCode()

        return result
    }
}
