package com.williamdubail.bankingtest.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.williamdubail.bankingtest.data.model.Resource
import kotlinx.coroutines.flow.Flow

@Dao
interface ResourceDao {

    @Query("DELETE FROM resource")
    fun delete()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(resourceList: List<Resource>): List<Long>

    @Query("SELECT * FROM resource")
    fun get(): Flow<List<Resource>>

    @Query("SELECT * FROM resource WHERE parenturi = :parentUri")
    fun getResourceFromParent(parentUri: String): Flow<List<Resource>>
}