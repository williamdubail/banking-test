package com.williamdubail.bankingtest.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.williamdubail.bankingtest.BuildConfig
import com.williamdubail.bankingtest.data.database.dao.ResourceDao
import com.williamdubail.bankingtest.data.model.Resource

@Database(entities = [Resource::class], version = 3, exportSchema = false)
abstract class DatabaseRoom : RoomDatabase() {

    abstract fun resourceDao(): ResourceDao

    companion object {

        @Volatile
        private var INSTANCE: DatabaseRoom? = null

        fun getDataBase(context: Context): DatabaseRoom {

            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(context, DatabaseRoom::class.java, BuildConfig.APPLICATION_ID)
                    .fallbackToDestructiveMigration().build()

                INSTANCE = instance
                instance
            }
        }
    }
}