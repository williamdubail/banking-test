package com.williamdubail.bankingtest.data.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Api private constructor() {

    companion object {

        val SUCCESS: IntRange = 200..299
        const val UNPROCESSABLE_ENTITY = 422
        const val INTERNAL_SERVER_ERROR = 500

        private var instance: Api = Api()

        fun instance(): Api {
            return instance
        }
    }


    var retrofit: Retrofit = Retrofit.Builder()
        .baseUrl("https://raw.githubusercontent.com/bankin-engineering/challenge-android/master/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()


}