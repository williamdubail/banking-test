package com.williamdubail.bankingtest.data.model

sealed class StateData<out T> {

    object Loading : StateData<Nothing>()

    data class Error(var message : String) : StateData<Nothing>()

    data class Data<T>(var data: T) : StateData<T>()
}