package com.williamdubail.bankingtest.data.api.service

import com.williamdubail.bankingtest.data.model.Resource
import com.williamdubail.bankingtest.data.model.ResourceResponse
import retrofit2.Response
import retrofit2.http.GET

interface ResourceService {

    @GET("categories.json")
    suspend fun resourceList(): Response<ResourceResponse>

}