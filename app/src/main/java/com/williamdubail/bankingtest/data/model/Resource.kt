package com.williamdubail.bankingtest.data.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


/**

{
"id": 441988,
"resource_uri": "/v2/categories/441988",
"resource_type": "category",
"name": "TVA",
"parent":
{
"id": 159,
"resource_uri": "/v2/categories/159",
"resource_type": "category"
},
"custom": false,
"other": false,
"is_deleted": false
}
 */

@Entity
data class Resource(

    @Expose
    @PrimaryKey
    var id: Long,

    @SerializedName("resource_uri")
    var uri: String,

    @SerializedName("resource_type")
    var type: String,

    @Expose
    var name: String,

    @Expose
    @Embedded(prefix = "parent")
    var parent: ResourceParent?,

    @Expose
    var custom: Boolean,

    @Expose
    var other: Boolean,

    @Expose
    var isDeleted: Boolean,
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Resource) return false

        if (id != other.id) return false
        if (uri != other.uri) return false
        if (type != other.type) return false
        if (name != other.name) return false
        if (parent != other.parent) return false
        if (custom != other.custom) return false
        if (other != other.other) return false
        if (isDeleted != other.isDeleted) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + uri.hashCode()
        result = 31 * result + type.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + parent.hashCode()
        result = 31 * result + custom.hashCode()
        result = 31 * result + other.hashCode()
        result = 31 * result + isDeleted.hashCode()
        return result
    }
}
