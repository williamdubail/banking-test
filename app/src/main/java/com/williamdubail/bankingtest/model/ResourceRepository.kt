package com.williamdubail.bankingtest.model

import android.content.Context
import androidx.lifecycle.asLiveData
import androidx.lifecycle.liveData
import com.williamdubail.bankingtest.R
import com.williamdubail.bankingtest.data.api.Api
import com.williamdubail.bankingtest.data.api.service.ResourceService
import com.williamdubail.bankingtest.data.database.dao.ResourceDao
import com.williamdubail.bankingtest.data.model.Resource
import com.williamdubail.bankingtest.data.model.StateData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect

class ResourceRepository(
    private var context: Context,
    private var resourceDao: ResourceDao,
    private var resourceService: ResourceService
) {

    fun getResourceList() = liveData(Dispatchers.IO) {
        emit(StateData.Loading)
        emitSource(refreshDataResourceListApi())

        resourceDao.get().collect {
            if (it.isNotEmpty())
                emit(StateData.Data(it))
        }


    }


    fun getResourceFromParent(resource: Resource) = liveData(Dispatchers.IO) {
        emit(StateData.Loading)

        resource.parent?.uri.let {
            if (it.isNullOrEmpty())
                emit(StateData.Data(arrayListOf<Resource>()))
            else
                resourceDao.getResourceFromParent(it).collect{ resourceList ->
                    emit(StateData.Data(resourceList))
                }

        }


    }


    private fun refreshDataResourceListApi() = liveData(Dispatchers.IO) {
        resourceService.resourceList().let {
            if (Api.SUCCESS.contains(it.code())) {
                val resourceList = it.body()?.resourceList ?: arrayListOf()

                resourceDao.delete()
                resourceDao.add(resourceList)

                emit(StateData.Data(resourceList))

            } else if (arrayListOf(Api.INTERNAL_SERVER_ERROR, Api.UNPROCESSABLE_ENTITY).contains(it.code()))
                emit(StateData.Error(it.errorBody()?.toString() ?: context.getString(R.string.error_api)))
            else
                emit(StateData.Error(""))
        }
    }


}