package com.williamdubail.bankingtest.ui.fragments.resourceList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.williamdubail.bankingtest.data.model.Resource
import com.williamdubail.bankingtest.data.model.StateData
import com.williamdubail.bankingtest.databinding.FragmentResourceListBinding
import com.williamdubail.bankingtest.ui.MainActivity
import com.williamdubail.bankingtest.ui.adapter.ResourceAdapter
import com.williamdubail.bankingtest.ui.fragments.resourceDetail.ResourceDetailFragment

class ResourceListFragment : Fragment(), ResourceAdapter.OnResourceListener {

    companion object {
        fun instance(): ResourceListFragment = ResourceListFragment()
    }


    private var binding: FragmentResourceListBinding? = null
    private lateinit var resourceViewModel: ResourceListViewModel
    private lateinit var adapter: ResourceAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        resourceViewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(requireActivity().application)
            .create(ResourceListViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentResourceListBinding.inflate(inflater, container, false)
        binding?.lifecycleOwner = viewLifecycleOwner
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = ResourceAdapter(this)

        binding?.rvResourceList?.layoutManager = LinearLayoutManager(requireContext())
        binding?.rvResourceList?.adapter = adapter

        resourceViewModel.getResourceList().observe(viewLifecycleOwner) {

            when (it) {
                is StateData.Data -> {

                    binding?.isLoading = false
                    adapter.updateResource(it.data)
                }

                is StateData.Error -> {

                    binding?.isLoading = false

                    if (it.message.isNotEmpty())
                        (activity as MainActivity).showSnackBar(it.message)
                }

                StateData.Loading -> binding?.isLoading = true
            }
        }


    }


    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    override fun onDetail(resource: Resource) {
        val resourceDetailFragment = ResourceDetailFragment.instance()
        resourceDetailFragment.resource = resource

        (activity as MainActivity).addFragment(resourceDetailFragment)

    }
}