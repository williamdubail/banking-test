package com.williamdubail.bankingtest.ui.fragments.resourceDetail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.williamdubail.bankingtest.data.api.Api
import com.williamdubail.bankingtest.data.api.service.ResourceService
import com.williamdubail.bankingtest.data.database.DatabaseRoom
import com.williamdubail.bankingtest.data.database.dao.ResourceDao
import com.williamdubail.bankingtest.data.model.Resource
import com.williamdubail.bankingtest.model.ResourceRepository

class ResourceDetailViewModel(var app: Application) : AndroidViewModel(app) {


    private var resourceDao: ResourceDao = DatabaseRoom.getDataBase(app).resourceDao()
    private var resourceService = Api.instance().retrofit.create(ResourceService::class.java)

    private var resourceRepository: ResourceRepository = ResourceRepository(app, resourceDao, resourceService)


    fun getResourceFromParent(resource: Resource) = resourceRepository.getResourceFromParent(resource)

}