package com.williamdubail.bankingtest.ui.diffUtil

import androidx.recyclerview.widget.DiffUtil
import com.williamdubail.bankingtest.data.model.Resource

class ResourceDiffUtil : DiffUtil.ItemCallback<Resource>() {

    override fun areItemsTheSame(oldItem: Resource, newItem: Resource) =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Resource, newItem: Resource) =
        oldItem == newItem
}