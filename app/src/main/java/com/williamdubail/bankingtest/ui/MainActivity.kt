package com.williamdubail.bankingtest.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.snackbar.Snackbar
import com.williamdubail.bankingtest.databinding.ActivityMainBinding
import com.williamdubail.bankingtest.ui.fragments.resourceList.ResourceListFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        addFragment(ResourceListFragment.instance())
    }

    fun addFragment(fragment: Fragment) {
        fragment.retainInstance = true
        supportFragmentManager.beginTransaction()
            .addToBackStack(null)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            .add(binding.fcvMain.id, fragment, fragment.javaClass.simpleName)
            .commit()
    }

    fun showSnackBar(message: String) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
    }

}