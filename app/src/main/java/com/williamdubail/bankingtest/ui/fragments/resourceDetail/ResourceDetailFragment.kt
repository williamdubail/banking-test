package com.williamdubail.bankingtest.ui.fragments.resourceDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.williamdubail.bankingtest.data.model.Resource
import com.williamdubail.bankingtest.data.model.StateData
import com.williamdubail.bankingtest.databinding.FragmentResourceDetailBinding
import com.williamdubail.bankingtest.ui.MainActivity
import com.williamdubail.bankingtest.ui.adapter.ResourceAdapter

class ResourceDetailFragment : Fragment() {

    companion object {
        fun instance() = ResourceDetailFragment()
    }

    private var binding: FragmentResourceDetailBinding? = null
    private lateinit var resourceViewModel: ResourceDetailViewModel
    private lateinit var adapter: ResourceAdapter

    lateinit var resource: Resource

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        resourceViewModel = ViewModelProvider.AndroidViewModelFactory.getInstance(requireActivity().application)
            .create(ResourceDetailViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentResourceDetailBinding.inflate(inflater, container, false)
        binding?.lifecycleOwner = viewLifecycleOwner

        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = ResourceAdapter()
        binding?.resource = resource

        binding?.rvResourceList?.layoutManager = LinearLayoutManager(requireContext())
        binding?.rvResourceList?.adapter = adapter

        binding?.mtbResourceDetail?.setNavigationOnClickListener {
            activity?.onBackPressed()
        }

        resourceViewModel.getResourceFromParent(resource).observe(viewLifecycleOwner) {

            when (it) {
                is StateData.Data -> {
                    binding?.isLoading = false
                    binding?.isEmpty = it.data.isEmpty()
                    adapter.updateResource(it.data)
                }
                is StateData.Error -> {
                    binding?.isLoading = false

                    if (it.message.isNotEmpty())
                        (activity as MainActivity).showSnackBar(it.message)
                }
                StateData.Loading -> binding?.isLoading = true
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }
}