package com.williamdubail.bankingtest.ui

import android.view.View
import androidx.databinding.BindingAdapter

class BindingAdapter {

    companion object {
        @JvmStatic
        @BindingAdapter("isShow")
        fun View.isShow(isShow: Boolean) {
            visibility = if (isShow)
                View.VISIBLE
            else
                View.GONE
        }
    }
}