package com.williamdubail.bankingtest.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.williamdubail.bankingtest.data.model.Resource
import com.williamdubail.bankingtest.databinding.ItemResourceBinding
import com.williamdubail.bankingtest.ui.diffUtil.ResourceDiffUtil


class ResourceAdapter(private var onResourceListener: OnResourceListener? = null) :
    ListAdapter<Resource, ResourceAdapter.ResourceViewHolder>(ResourceDiffUtil()) {

    fun updateResource(resourceList: List<Resource>) {
        submitList(resourceList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResourceViewHolder {
        val itemResourceBinding = ItemResourceBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ResourceViewHolder(itemResourceBinding)
    }

    override fun onBindViewHolder(holder: ResourceViewHolder, position: Int) {
        holder.itemResourceBinding.clItem.setOnClickListener {
            this.onResourceListener?.onDetail(currentList[position])
        }
        return holder.bind(currentList[position])
    }

    class ResourceViewHolder(var itemResourceBinding: ItemResourceBinding) :
        RecyclerView.ViewHolder(itemResourceBinding.root) {

        fun bind(resource: Resource) {
            itemResourceBinding.resource = resource
        }
    }

    interface OnResourceListener {
        fun onDetail(resource: Resource)
    }
}